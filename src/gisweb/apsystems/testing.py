# -*- coding: utf-8 -*-
from plone.app.robotframework.testing import REMOTE_LIBRARY_BUNDLE_FIXTURE
from plone.app.testing import applyProfile
from plone.app.testing import FunctionalTesting
from plone.app.testing import IntegrationTesting
from plone.app.testing import PLONE_FIXTURE
from plone.app.testing import PloneSandboxLayer
from plone.testing import z2

import gisweb.apsystems


class GiswebApsystemsLayer(PloneSandboxLayer):

    defaultBases = (PLONE_FIXTURE,)

    def setUpZope(self, app, configurationContext):
        self.loadZCML(package=gisweb.apsystems)

    def setUpPloneSite(self, portal):
        applyProfile(portal, 'gisweb.apsystems:default')


GISWEB_apsystems_FIXTURE = GiswebApsystemsLayer()


GISWEB_APSYSTEMS_INTEGRATION_TESTING = IntegrationTesting(
    bases=(GISWEB_apsystems_FIXTURE,),
    name='GiswebApsystemsLayer:IntegrationTesting'
)


GISWEB_apsystems_FUNCTIONAL_TESTING = FunctionalTesting(
    bases=(GISWEB_apsystems_FIXTURE,),
    name='GiswebApsystemsLayer:FunctionalTesting'
)


GISWEB_apsystems_ACCEPTANCE_TESTING = FunctionalTesting(
    bases=(
        GISWEB_apsystems_FIXTURE,
        REMOTE_LIBRARY_BUNDLE_FIXTURE,
        z2.ZSERVER_FIXTURE
    ),
    name='GiswebApsystemsLayer:AcceptanceTesting'
)
