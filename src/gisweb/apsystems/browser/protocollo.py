# -*- coding: utf-8 -*-
from Products.Five.browser import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from zope.interface import implements
#copiata qui in attesa di migrazione
#from iol.document.utils.plomino_utils import getAttachmentInfo
from iol.document.utils.serialdoc import getAttachmentsInfo
from zope.component import getMultiAdapter

from suds.client import Client
from suds.xsd.doctor import Import, ImportDoctor
from suds.sax.text import Raw
from suds.plugin import MessagePlugin
from xml.sax.saxutils import escape

import logging
import os, base64, uuid
import simplejson as json
import datetime



_logger = logging.getLogger('gisweb.apsystems')

class LogPlugin(MessagePlugin):
  def sending(self, context):
    print(str(context.envelope))
  def received(self, context):
    print(str(context.reply))


class ApsystemsProtocollo(BrowserView):

    def __init__(self, context, request):

        self.context = context
        self.request = request
        self.document = context
        self.config = {}
        self.docField = request.get('docfield',None)
        self.formName = request.get('formname',None)
        self.message=''
        self.attachmentInfo = {}
        self.mittente = {}
        self.destinatario = {}

    def getSettings(self):
        """
        Completa la configurazione con la sostituzione dei nomi dei campi plomino con i corrispondenti valori
        """

        doc = self.document
        db = self.document.getParentDatabase()
        if not "config_protocollo_apsystems" in db.resources.keys():
            self.message = "MANCA LA CONFIGURAZIONE"
            return False

        self.config = db.resources.config_protocollo_apsystems()
        mittente = self.config.get("mittente")
        #sostituisco ai nomi dei campi i valori
        for field in mittente:
            mittente[field] = doc.getItem(mittente[field])
        mittente["descrizione"] = "%s %s" %(doc.getItem("fisica_cognome"),doc.getItem("fisica_nome"))
        mittente["indirizzo"] = "%s %s" %(doc.getItem("fisica_indirizzo"),doc.getItem("fisica_civico"))

        self.mittente = mittente
        self.destinatario = self.config.get("destinatario")
        return True

    def protocollaDocumento(self, messageData=None):
        """
        messageData solo per protocollazione in uscita
        con testxml=1 restituisce il documento xml
        vecchia versione
        .../<<documento>>/@@protocolla_documento?docfield=comunicazione_digitale&testxml=1
        """

        doc=self.context
        db=doc.getParentDatabase()
        testxml = 0

        # se manca il config_protocollo esco
        if not 'config_protocollo' in db.resources:
            message = u"Manca la configurazione del protocollo in resources"
            return {'success':0,'message':message}

        config = db.callScriptMethod('config_protocollo','getConfig',doc)

        if messageData:
            # passo messageData solo per la protocollazione in uscita
            if isinstance(messageData, basestring):
                messageData = json.loads(messageData)

            principale = messageData.get("principale")
            allegati = messageData.get("allegati")
            oggetto = messageData.get("protocollo_oggetto")
            destinatari_a = messageData.get('destinatari_a', [])
            destinatari_aoo = messageData.get('destinatari_aoo', [])
            destinatari_cc = messageData.get('destinatari_cc', [])
            flusso = 'U'
            testxml = messageData.get('testxml', 0)

            # se il flusso è in uscita i destinatari sono obbligatori
            if not (destinatari_a or destinatari_aoo):
                message = u"Mancano i destinatari per la protocollazione"
                return {'success': 0, 'message': message}

            # Patch per Halley
            # Bisogna mettere gli aoo in nei destinatari altrimenti non funziona invio a soli enti
            for aoo in destinatari_aoo:
                destinatari_a.append(dict(
                    nome="",
                    cognome=aoo.get("denominazione"),
                    codicefiscale=aoo.get("descrizione"),
                    indirizzo=aoo.get("indirizzo")
                ))
            ## ci vanno anche quelli in cc?
            self.Destinatari = destinatari_a + destinatari_cc
            self.DestinatariAOO = destinatari_aoo
            self.DestinatariA = destinatari_a
            self.DestinatariCC = destinatari_cc

        else:
            ### prot in entrata
            flusso = 'E'
            self.Mittente = config.get("Mittente")
            oggetto = config.get('Oggetto', '')
            principale = config.get('Principale', '')
            allegati = config.get('Allegati', [])

        attachmentInfo = []
        documentDate = datetime.datetime.today().strftime("%d/%m/%Y %H:%M")

        self.Oggetto = oggetto or doc.Title()
        self.Amministrazione = config.get("Amministrazione")

        wsUrl = config.get("wsUrl")
        #wsDiz = self.config.get("wsDiz")

        #userName = self.document.getCurrentUserId()

        #Il servizio protocolla anche senza allegato
        imp = Import('http://www.w3.org/2001/XMLSchema',location='http://www.w3.org/2001/XMLSchema.xsd')
        imp.filter.add("http://services.apsystems.it/")

        try:
            self.wsClient = Client(wsUrl, plugins=[ImportDoctor(imp)])
        except Exception, e:
            return {'success': 0, 'message': e}


        #Login
        loginInfo = config.get("login")
        auth = self.wsClient.factory.create('AuthenticationDetails')
        auth.UserName = loginInfo.get("username")
        auth.Password = loginInfo.get("password")
        self.wsClient.set_options(soapheaders=auth)
        userProtocollo = loginInfo.get("userprotocollo")


        #Definizione Documento
        try:
            tplProtocollo = ViewPageTemplateFile("templates/segnatura_%s.pt" % flusso.lower())
            xmlDoc = tplProtocollo(self)
        except Exception, e:
            return {'success': 0, 'message': e}
        #import pdb;pdb.set_trace()
        ############# PACIUGO DA CANCELLARE MA FUNZIONA ??##############
        #schema xml di imput. Lo prendo cosi perche il template mi setta tutti gli attributi in minuscolo
        path = os.path.dirname(os.path.abspath(__file__))
        f=open(path + '/inputSchema.xml','r')
        content = f.read()
        f.close()
#        oggetto = self.document.getItem('descrizione_intervento','MANCA DESCRIZIONE INTERVENTO')
        xmlDoc = content.replace('[OGGETTO]',escape(oggetto[:150]))
        xmlDoc = xmlDoc.replace('[MITTENTE]',escape(self.Mittente['Nominativo']))
        xmlDoc = xmlDoc.replace('[MITTENTE_INDIRIZZO]',escape(self.Mittente['Indirizzo']))
        xmlDoc = xmlDoc.replace('[MITTENTE_CAP]',self.Mittente['Cap'])
        xmlDoc = xmlDoc.replace('[MITTENTE_COMUNE]',self.Mittente['Comune'])



        #import pdb;pdb.set_trace()

        if self.request.get("testxml"):
            return xmlDoc

        try:
            result = self.wsClient.service.InsertProtocolloGenerale(dsDati=Raw(xmlDoc), userName=userProtocollo)

        except Exception, e:
            message =  e
            return {'success':0,'message':message}


        if not "diffgram" in result:
            return {'success':0, 'error':result.lngErrNumber, 'message':result.strErrString}

        res = result["diffgram"]

        if 'errori' in res:
            return {'success':0,'message':res["errori"]["errore"]["descrizione"]}

        if 'protocolli' in res:
            protocollo = res["protocolli"]["protocollo"]
        else:
            return {'success':0, 'message':'Errore nella restituzione del protocollo'}

        #import pdb;pdb.set_trace()
        #Inserimento dell'allegato:

        attachmentInfo = getAttachmentsInfo(doc, principale)
        fileInfo = attachmentInfo[0]
        try:
            self.wsClient.service.InsertAllegatoProtocolloGenerale(
                codice=protocollo["codice"],
                fileBytes=base64.b64encode(fileInfo["content"]),
                fileName=fileInfo["name"],
                userName=userProtocollo)

        except Exception, e:
            f=open(path + '/log.xml','w')
            f.write(self.wsClient.last_sent().str())
            f.close()
            message =  e
            #Da aggiungere problemi con l'inserimento degli allegati. Non si può uscire con un protocollo staccato


        # protocollazione altri file allegati
        self.fileAllegati=[]
        allegatiInfo = getAttachmentsInfo(doc,allegati)
        for fileInfo in allegatiInfo:
            try:
                self.wsClient.service.InsertAllegatoProtocolloGenerale(
                    codice=protocollo["codice"],
                    fileBytes=fileInfo["content"],
                    fileName=fileInfo["name"],
                    userName=userProtocollo)

            except Exception, e:
                f = open(path + '/log.xml', 'w')
                f.write(self.wsClient.last_sent().str())
                f.close()
                message = e
                #Da aggiungere problemi con l'inserimento degli allegati. Non si può uscire con un protocollo staccato
                #return {'success': 0, 'message': message}

        return {'success':1,'NumeroProtocollo':int(float(protocollo["numero_protocollo"])),'DataProtocollo':protocollo["data_protocollo"],'OraProtocollo':protocollo["ora_protocollo"],'CodiceProtocollo':protocollo["codice"]}


    def __call__(self):
        """
        DA rivedere come gestire l'output
        """
        result = self.protocollaDocumento()
        if not result:
            return self.message

        return result
