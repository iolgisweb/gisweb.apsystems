# -*- coding: utf-8 -*-
"""Setup tests for this package."""
from gisweb.apsystems.testing import GISWEB_APSYSTEMS_INTEGRATION_TESTING  # noqa
from plone import api

import unittest2 as unittest


class TestSetup(unittest.TestCase):
    """Test that gisweb.apsystems is properly installed."""

    layer = GISWEB_APSYSTEMS_INTEGRATION_TESTING

    def setUp(self):
        """Custom shared utility setup for tests."""
        self.portal = self.layer['portal']
        self.installer = api.portal.get_tool('portal_quickinstaller')

    def test_product_installed(self):
        """Test if gisweb.apsystems is installed with portal_quickinstaller."""
        self.assertTrue(self.installer.isProductInstalled('gisweb.apsystems'))

    def test_browserlayer(self):
        """Test that IGiswebApsystemsLayer is registered."""
        from gisweb.apsystems.interfaces import IGiswebApsystemsLayer
        from plone.browserlayer import utils
        self.assertIn(IGiswebApsystemsLayer, utils.registered_layers())


class TestUninstall(unittest.TestCase):

    layer = GISWEB_APSYSTEMS_INTEGRATION_TESTING

    def setUp(self):
        self.portal = self.layer['portal']
        self.installer = api.portal.get_tool('portal_quickinstaller')
        self.installer.uninstallProducts(['gisweb.apsystems'])

    def test_product_uninstalled(self):
        """Test if gisweb.apsystems is cleanly uninstalled."""
        self.assertFalse(self.installer.isProductInstalled('gisweb.apsystems'))
